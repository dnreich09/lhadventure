# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
import unittest
import gamelib.scene
import gamelib.scene_object
import pygame

class TestObject(gamelib.scene_object.SceneObject):
    def on_add(self, parent):
        parent.listen('test', self.test)

    def test(self):
        self.groups()[0].tests += 1

class TestScene(unittest.TestCase):

    def setUp(self):
        self.scene = gamelib.scene.Scene()
        self.scene.tests = 0

    def test_add(self):
        obj = gamelib.scene_object.SceneObject(pygame.Surface((50, 50)))
        self.scene.add(obj)

        self.assertIn(obj, self.scene)

    def test_remove(self):
        obj = gamelib.scene_object.SceneObject(pygame.Surface((50, 50)))
        self.scene.add(obj)

        self.assertIn(obj, self.scene)

        self.scene.remove(obj)

        self.assertNotIn(obj, self.scene)

    def test_remove_with_events(self):
        test_obj = TestObject(pygame.Surface((50, 50)))
        self.scene.add(test_obj)
        # TestObject registers a function which will increment the scene's tests variable by 1
        self.scene.dispatch_event('test')
        self.assertTrue(self.scene.tests == 1)
        test_obj.kill()
        self.scene.dispatch_event('test')
        # tests should still only equal 1
        self.assertTrue(self.scene.tests == 1)