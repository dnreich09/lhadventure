# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
import unittest
import gamelib.animation_set

class TestAnimationSet(unittest.TestCase):

    def setUp(self):
        self.animation_set = gamelib.animation_set.AnimationSet(None, 32, 32, 4, 3, 1,
                            {"walking":[0, 1, 2, 3], "running":[4, 5, 6, 7], "idle":[8, 9, 10, 11]})

    def test_getframerect(self):
        rect = self.animation_set.get_framerect(3)
        rect2 = self.animation_set.get_framerect(5)

        self.assertTrue(rect.topleft == (99, 0))
        self.assertTrue(rect.width == 32 and rect.height == 32)

        self.assertTrue(rect2.topleft == (33, 33))
