# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
import unittest
import gamelib.event

class TestEventDispatcher(unittest.TestCase):

    def setUp(self):
        self.event_dispatcher = gamelib.event.EventDispatcher()

    def test_listen(self):
        self.event_dispatcher.listen('on_click', self.on_click)

        self.assertIn(self.on_click, self.event_dispatcher.event_types['on_click'])

    def test_deafen(self):
        self.event_dispatcher.listen('on_click', self.on_click)

        self.assertTrue(self.event_dispatcher.deafen('on_click', self.on_click))
        self.assertNotIn('on_click', self.event_dispatcher.event_types)

        self.event_dispatcher.listen('on_test', self.on_click)
        self.event_dispatcher.listen('on_test', self.on_test)
        self.event_dispatcher.deafen('on_test', self.on_click)

        self.assertNotIn(self.on_click, self.event_dispatcher.event_types['on_test'])

    def test_dispatch_events(self):
        self.event_dispatcher.listen('on_click', self.on_click)
        self.event_dispatcher.listen('on_test', self.on_test)

        self.assertTrue(self.event_dispatcher.dispatch_event('on_click', 'left', (50, 20)))
        self.assertTrue(self.event_dispatcher.dispatch_event('on_test'))
        self.assertFalse(self.event_dispatcher.dispatch_event('false_event'))
        self.assertTrue(self.testing)
        self.assertEqual(self.button, 'left')
        self.assertEqual(self.pos, (50, 20))

    def on_click(self, button, pos):
        self.button = button
        self.pos = pos

    def on_test(self):
        self.testing = True
