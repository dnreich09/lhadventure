# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
import unittest

import gamelib.director
import gamelib.scene

class TestDirector(unittest.TestCase):

    def setUp(self):
        self.director = gamelib.director.Director()
        self.scene1 = gamelib.scene.Scene()
        self.scene2 = gamelib.scene.Scene()

    def test_replace_empty(self):
        self.assertRaises(IndexError, self.director.replace, self.scene1)

    def test_push(self):
        self.director.push(self.scene1)
        self.director.push(self.scene2)

        self.assertTrue(len(self.director._scene_stack) == 2)
        self.assertEqual(self.scene2, self.director.scene)

    def test_pop(self):
        self.assertRaises(IndexError, self.director.pop)

        self.director.push(self.scene1)
        self.director.push(self.scene2)

        self.assertEqual(self.scene2, self.director.pop())
        self.assertTrue(len(self.director._scene_stack) == 1)
        self.assertEqual(self.scene1, self.director.scene)
        self.assertRaises(IndexError, self.director.pop)

    def test_replace(self):
        self.assertRaises(IndexError, self.director.replace, self.scene1)

        self.director.push(self.scene1)
        self.director.replace(self.scene2)

        self.assertEqual(self.scene2, self.director.scene)
        self.assertTrue(len(self.director._scene_stack) == 1)