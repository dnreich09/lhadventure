# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
import math
import pygame
import gamelib.vector

class SceneObject(pygame.sprite.Sprite):
    def __init__(self, base_image=None, animation_set=None, pos=(0,0), manipulation='topleft'):
        """ Initialize a scene object. If a base image is provided, the object's rect attributes will be based
        from that. If one is not provided, the user may specify an animation_set which will be used instead.

        Keyword arguments:
        base_image = A pygame surface containing the base image which will be rendered.
        animation_set = AnimationSet to use for rendering.
        pos = Position of the scene object.
        manipulation = The point of manipulation for scene object's translation and rotation methods.

        """
        self.parent = None
        self.animation = animation_set
        self._base_image = base_image
        if animation_set:
            self.rect = pygame.Rect(0, 0, animation_set.frame_width, animation_set.frame_height)
        elif base_image:
            self.rect = self._base_image.get_rect()
            self._image = self._base_image.copy()
        self._manipulation = manipulation
        self.angle = 0
        self.position = gamelib.vector.Vec2d(pos)
        setattr(self.rect, manipulation, pos)

        pygame.sprite.Sprite.__init__(self)

    @property
    def image(self):
        if self.animation:
            return self.animation.current_frame
        return self._image

    def move(self, *position):
        """ Move object to specified position. """
        self.position = gamelib.vector.Vec2d(position)
        setattr(self.rect, self._manipulation, position)

    def translate(self, *vector):
        """ Translate object by specified vector. """
        self.position += vector
        setattr(self.rect, self._manipulation, self.position)

    def flip(self, x, y):
        if self.animation:
            self.animation.flip(x, y)
        else:
            self._image = pygame.transform.flip(self._image, x, y)

    def rotate(self, degrees):
        """ Rotate object by specified degrees. """
        self.angle += degrees
        if self.angle > 360:
            self.angle -= 360
        elif self.angle < 0:
            self.angle += 360

        if self.animation:
            self.animation.rotate(self.angle)
        else:
            self._image = pygame.transform.rotate(self._base_image, self.angle)

        self.rect = self.image.get_rect(center=self.rect.center)

    def on_add(self, parent):
        # TODO: Have on_add call the scene object's attached script, if one exists.
        # TODO: Such a script will be responsible for registering event interests with
        # TODO: the parent scene, and setting initial attributes.
        pass

    def update(self, dt):
        if self.animation:
            self.animation.update(dt)

        # Make sure the position matches the game object's rect attribute.
        # The reason for this is because the only way position should mutate
        # state is by way of the move() and translate() object methods. All
        # other changes happen by way of rect manipulation.
        if self.position.floored() != getattr(self.rect, self._manipulation):
            self.position = gamelib.vector.Vec2d(getattr(self.rect, self._manipulation))
