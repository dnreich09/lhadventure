# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
class EventDispatcher(object):

    def __init__(self):
        self.event_types = {}

    def dispatch_event(self, event_type, *args):
        """ Dispatch an event to registered handlers.

        Keyword arguments:
        event_type = The type of event to dispatch.
        *args = Any arguments needing to be passed to the event handler.
        """
        try:
            for listener in self.event_types[event_type]:
                listener(*args)
        except KeyError:
            return False

        return True

    def listen(self, event_type, callback_func):
        """ Register a listener with an event type. If event type
            is not known, add type to dictionary.

        Keyword arguments:
        event_type = Event type listener is interested in.
        callback_func = Function or method to call when event is
                        dispatched.

        """
        try:
            self.event_types[event_type].append(callback_func)
        except KeyError:
            self.event_types[event_type] = [callback_func]

        return True

    def deafen(self, event_type, callback_func):
        """ Remove a listener from an event type.

        Keyword arguments:
        event_type = Event type listener wants to detach from.
        callback_func = Function or method listener wants to
                        deafen.

        """
        try:
            self.event_types[event_type].remove(callback_func)
        except KeyError:
            return False
        except ValueError:
            return False

        if not self.event_types[event_type]:
            del self.event_types[event_type]

        return True