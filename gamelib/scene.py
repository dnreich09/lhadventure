# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
import pygame
import gamelib.event

#TODO: Use multiple groups for scene management. Can keep the LayeredUpdates group for rendering,
#TODO: and use a separate group for non-rendering scene objects. Spatial hashing will be done in
#TODO: a group of it's own as well? Helps provide an additional layer of abstraction. Decisions decisions...
class Scene(pygame.sprite.LayeredUpdates, gamelib.event.EventDispatcher):
    def __init__(self, *sprites, **kwargs):

        pygame.sprite.LayeredUpdates.__init__(self, *sprites, **kwargs)
        gamelib.event.EventDispatcher.__init__(self)

        self.on_create()

    def add_internal(self, scene_object, layer=None):
        scene_object.on_add(self)
        scene_object.parent = self

        pygame.sprite.LayeredUpdates.add_internal(self, scene_object, layer)

    def remove_internal(self, scene_object):
        """ Make sure to deafen any events the object is listening to. """
        scene_object.parent = None

        for key in self.event_types.keys():
            for func in self.event_types[key]:
                if hasattr(func, 'im_self') and func.im_self is scene_object:
                    self.deafen(key, func)

        pygame.sprite.LayeredUpdates.remove_internal(self, scene_object)

    def on_create(self):
        # TODO: Call attached script to initialize any event interests with self, set
        # TODO: attributes, etc...
        pass

    def draw(self, surface):
        """ Draw the entire scene.

        Keyword arguments:
        surface = The display surface to draw on.

        """
        pygame.sprite.LayeredUpdates.draw(self, surface)