# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
class Animation(object):

    def __init__(self, sheet_image, frame_width, frame_height, border_width, width, height, total_frames):
        self.sheet_image = sheet_image
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.border_width = border_width
        self.width = width
        self.height = height
        self.total_frames = total_frames
