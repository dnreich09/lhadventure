# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import os
import json
import pygame
import gamelib.animation_set

data_py = os.path.abspath(os.path.dirname(__file__))
data_dir = os.path.normpath(os.path.join(data_py, '..', 'data'))

def load_sound(sound_name, extension='wav'):
    """ Load a sound file and return a pygame Sound object.

    Keyword arguments:
    sound_name = filename of sound to load
    extension = filename extension, wav by default
    """
    return pygame.mixer.Sound(os.path.join(data_dir, 'sound', 'effects', sound_name + '.' + extension))

def load_music(song_name, extension='ogg'):
    """ Load a music file into the pygame mixer.

    Keyword arguments:
    song_name = filename of song to load
    extension = filename extension, ogg by default
    """
    pygame.mixer.music.load(os.path.join(data_dir, 'sound', 'music', song_name + '.' + extension))

def load_image(image_name, color_key=None, alpha=False):
    """ Load an image from file and return a pygame surface converted to
        the display surface's  pixel format.

    Keyword arguments:
    image_name = filename of image to load
    color_key = Set a color key for the image. Pixels with the same
                color key will be transparent.
    alpha = If True, will optimize surface for fast alpha blitting.
    """
    image = pygame.image.load(os.path.join(data_dir, 'images', image_name))
    if alpha:
        return image.convert_alpha()
    if color_key:
        image.set_colorkey(color_key)
    return image.convert()

def load_animation_set(animation_file):
    """ Load an animation set from the specified file and return an Animation object.

    Keyword arguments:
    animation_file = JSON encoded file describing an animation set.

    """
    fp = open(os.path.join(data_dir, 'animations', animation_file + '.json'))
    data = json.load(fp)
    fp.close()

    image = load_image(data['image_file'], data['color_key'], data['use_alpha'])
    return gamelib.animation_set.AnimationSet(image, data['frame_width'], data['frame_height'], data['width'],
                                        data['height'], data['padding'], data['animation_list'])