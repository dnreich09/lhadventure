# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
import pygame
import platform
import time

system = platform.system()

class Director(object):

    def __init__(self):
        self._scene_stack = []
        self.surface = None
        self._running = False
        self._initialized = False
        self._sound = True
        self._elapsed_time = 0.0

    def initialize(self, resolution=(640,480), update_interval=0.01):
        """ Initialize and set up the director.

        Keyword arguments:
        resolution = resolution of window
        update_interval = fixed dt for game updates, ~100 updates per second default
        """
        if pygame.mixer:
            pygame.mixer.pre_init(44100, -16, 2, 1024)
        pygame.init()
        if not pygame.mixer or not pygame.mixer.get_init():
            # TODO: Implement proper logging here.
            print('Apparently, the DJ was a no call/no show.')
            print('In other words, there will be no sound.')
            self._sound = False

        self._update_interval = update_interval
        
        self.surface = pygame.display.set_mode(resolution)
        self._initialized = True

    @property
    def scene(self):
        """ Return current scene. """
        return self._scene_stack[-1]

    def push(self, scene):
        """ Push a new scene to the director's scene stack. """
        if self._scene_stack:
            self.scene.dispatch_event('scene_leave')
        self._scene_stack.append(scene)
        scene.dispatch_event('scene_enter')

    def pop(self):
        """ Pop from the stack and return the current scene. """
        if not self._scene_stack:
            raise IndexError('Popping from an empty stack.')
        elif len(self._scene_stack) == 1:
            raise IndexError('Popping the last scene results in an empty stack.')
        last_scene = self._scene_stack.pop()
        last_scene.dispatch_event('scene_leave')
        self.scene.dispatch_event('scene_enter')

        return last_scene

    def replace(self, scene):
        """ Replace the current scene with a new one. """
        if not self._scene_stack:
            raise IndexError('Trying to perform a replace on an empty scene stack.')
        self.scene.dispatch_event('scene_leave')
        self._scene_stack.pop()
        self._scene_stack.append(scene)
        self.scene.dispatch_event('scene_enter')

    def stop(self):
        """ Stop the game. (quit) """
        self._running = False

    def run(self):
        """ Run the game. """
        if not self._initialized:
            # TODO: Implement proper exception handling here.
            print('Generally, one would not run a game without initializing')
            print('the director first... Please come again.')
            return

        if not self._scene_stack:
            # TODO: Implement proper exception handling here.
            print('Your stack of scenes seems to be... lacking.')
            print('Please come again.')
            return

        if system == 'Windows':
            time.clock()
            current_time = 0.0
            tick = time.clock
        else:
            current_time = time.time()
            tick = time.time
        accumulator = 0.0
        
        self._running = True

        while self._running:

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self._running = False

                elif event.type == pygame.MOUSEMOTION:
                    self.scene.dispatch_event('mousemotion', event.pos, event.rel, event.buttons)
                elif event.type == pygame.MOUSEBUTTONUP:
                    self.scene.dispatch_event('mousebuttonup', event.pos, event.button)
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.scene.dispatch_event('mousebuttondown', event.pos, event.button)

                elif event.type == pygame.KEYDOWN:
                    self.scene.dispatch_event('keydown', event.unicode, event.key, event.mod)
                elif event.type == pygame.KEYUP:
                    self.scene.dispatch_event('keyup', event.key, event.mod)

                elif event.type == pygame.JOYAXISMOTION:
                    self.scene.dispatch_event('joyaxismotion', event.joy, event.axis, event.value)
                elif event.type == pygame.JOYBALLMOTION:
                    self.scene.dispatch_event('joyballmotion', event.joy, event.ball, event.rel)
                elif event.type == pygame.JOYHATMOTION:
                    self.scene.dispatch_event('joyhatmotion', event.joy, event.hat, event.value)
                elif event.type == pygame.JOYBUTTONUP:
                    self.scene.dispatch_event('joybuttonup', event.joy, event.button)
                elif event.type == pygame.JOYBUTTONDOWN:
                    self.scene.dispatch_event('joybuttondown', event.joy, event.button)

                elif event.type >= pygame.USEREVENT and event.type <= pygame.NUMEVENTS:
                    self.scene.dispatch_event(event.type, event)

            new_time = tick()
            frame_time = new_time - current_time
            current_time = new_time

            accumulator += frame_time

            while accumulator >= self._update_interval:
                accumulator -= self._update_interval
                self.scene.update(self._update_interval)
                self.scene.dispatch_event('update', self._update_interval)

            self.scene.draw(self.surface)
            pygame.display.flip()

            time.sleep(0.001)

director = Director()
