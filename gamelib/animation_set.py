# Copyright 2013 David Reichard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
import pygame

DEFAULT_SPEED = 0.3 # Default frame speed of 1 frame every 300 milliseconds.

class AnimationSet(object):

    def __init__(self, sheet_image, frame_width, frame_height, width, height, padding, animation_list):
        """ Initialize the animation object.

        Keyword arguments:
        sheet_image = Pygame surface containing the sprite sheet image.
        frame_width = Width of individual frame.
        frame_height = Height of individual frame.
        width = Count of how many frames wide on sheet.
        height = Count of how many frames tall on sheet.
        padding = Padding in pixels between each frame on sheet.
        animation_list = Dictionary of animation name to numeric sequence of frames.

        """
        self.base_sheet = sheet_image
        self._sheet = sheet_image.copy()
        self.frame_width = frame_width
        self.frame_height = frame_height
        self._width = width
        self._height = height
        self._padding = padding
        self._animation_list = animation_list

        self._current_animation_name = "None"
        self._current_animation = None
        self._frame_step = 0
        self._speed = DEFAULT_SPEED
        self._counter = 0
        self._loop = True
        self.running = False
        self._callback = None

        self.base_sprites = []
        for i in range(width * height):
            self.base_sprites.append(self.base_sheet.subsurface(self.get_framerect(i)))

        self.sprites = []
        for i in range(width * height):
            self.sprites.append(self._sheet.subsurface(self.get_framerect(i)))

    @property
    def name(self):
        return self._current_animation_name

    @property
    def current_frame(self):
        return self.sprites[self._current_animation[self._frame_step]]

    def flip(self, xbool, ybool):
        self.sprites = [pygame.transform.flip(x, xbool, ybool) for x in self.sprites]

    def rotate(self, degrees):
        """ Rotate sprites in sheet by specified degrees. """
        self.sprites = [pygame.transform.rotate(x, degrees) for x in self.base_sprites]

    def set(self, name):
        """ Set the animation to be played.

        Keyword arguments:
        name = Name of animation to be extracted from animation set.

        """
        self._current_animation = self._animation_list[name]
        self._current_animation_name = name

    def get_framerect(self, number):
        row = number / self._width
        column = number % self._width

        return pygame.Rect(self._padding * column + self.frame_width * column,
                            self._padding * row + self.frame_height * row,
                            self.frame_width, self.frame_height)

    def start(self, speed=DEFAULT_SPEED, loop=True, completion_callback=None):
        """ Begin playback of specified animation.

        Keyword arguments:
        speed = Gap in milliseconds between each frame change. Defaults to currently set speed.
        loop = True by default, specify whether animation should continuously loop.
        completion_callback = User defined callback to execute after animation completion.

        """
        self._speed = speed

        self._loop = loop
        self.running = True
        self._callback = completion_callback

    def stop(self):
        """ Stop playback of current animation. """
        self.running = False
        self._counter = 0
        self._callback = None

    def reset(self, step=0):
        """ Reset frame step to 0 by default or a user specified value.

        Keyword arguments:
        step = Frame step to reset to.

        """
        self._frame_step = step

    def update(self, dt):
        """ Update current animation.

        Keyword arguments:
        dt = Time in seconds since last update.

        """
        if self.running:
            self._counter += dt
            if self._counter >= self._speed:
                self._counter -= self._speed
                self._frame_step += 1

                if self._frame_step >= len(self._current_animation):
                    if not self._loop:
                        self._frame_step -= 1
                        self.stop()
                    else:
                        self._frame_step = 0

                    if self._callback:
                        self._callback()